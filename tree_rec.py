import os.path
import sys
import graphviz as gv
from Bio import AlignIO

counter = 0

if len(sys.argv) != 2:
    print "Incorrect input"
    exit(1)

file_name = sys.argv[1]
if not os.path.exists(file_name):
    print "File doesn't exist"
    exit(1)

nuc_dict = {"A": 0, "C": 1, "G": 2, "T": 3, "-": 4}
num_dict = {0: "A", 1: "C", 2: "G", 3: "T", 4: "-"}

h_nuc_dict = {'A':{'A': 0, 'C': 1, 'G': 2, 'T': 3},
            'C':{'A': 4, 'C': 5, 'G': 6, 'T': 7},
            'G':{'A': 8, 'C': 9, 'G': 10, 'T': 11},
            'T':{'A': 12, 'C': 13, 'G': 14, 'T': 15}}


handle = open(file_name, "rU")
align = AlignIO.read(handle, "clustal")

handle.close()

stat_file = open('./tree_stat.txt', "rw")
line = next(stat_file).split()
ls = [int(i) for i in line]
stat_file.close()


def build_tree(g, list_of_reads, parent, main_branch, first_iter=False):
    addition = 1 if first_iter else 0

    num_columns = len(align[0])
    dict_nuc_reads = {"A": [], "C": [], "G": [], "T": [], "-": []}
    # position, max_nuc
    current_error_pos = []

    error_list = []
    for i in range(num_columns):
        error_list.append([0]*5)
    for i in range(0, num_columns):
        for j in range(0, len(align)):
            if j not in list_of_reads:
                continue
            char = align[j][i]  # i-th symbol in j-th string
            ind = nuc_dict[char]
            error_list[i][ind] += 1

    error_numbers = 0
    for i in range(num_columns):
        cur_max = max(error_list[i])
        if cur_max < len(list_of_reads)-addition:
            error_numbers += 1

    if error_numbers == 0:
        return
    for i in range(num_columns):
        cur_max = max(error_list[i])
        index = error_list[i].index(cur_max)
        # i < num_columns
        # sub_max > 1
        gl_sec_max = 0
        if cur_max < len(list_of_reads) - addition:
            second_max = 0
            for ind, val in enumerate(error_list[i]):
                if second_max < val < cur_max:
                    second_max = val
            if second_max > gl_sec_max:
                dict_nuc_reads = {"A": [], "C": [], "G": [], "T": [], "-": []}
                gl_sec_max = second_max
                #
                tmp = [i, num_dict[index], cur_max]
                current_error_pos = tmp
                for j in range(len(align)):
                    if j in list_of_reads:
                        dict_nuc_reads[align[j][i]].append(j)

    global counter
    if len(current_error_pos) == 0:
        return
    for nuc in dict_nuc_reads:
        size = len(dict_nuc_reads[nuc])
        if size == 0:
            continue
        counter += 1
        if current_error_pos[1] != '-' and current_error_pos[1] != nuc and nuc != '-':
            ls[h_nuc_dict[current_error_pos[1]][nuc]] += 1
        label = str(current_error_pos[0])+' '+nuc+' '+str(size)
        g.node(str(counter), label)
        if size == current_error_pos[2] and main_branch:
            g.edge(str(parent), str(counter), _attributes={'color': 'green'})
            if error_numbers > 1:
                build_tree(graph, dict_nuc_reads[nuc], counter, 1)
        else:
            g.edge(str(parent), str(counter))
            if error_numbers > 1:
                build_tree(graph, dict_nuc_reads[nuc], counter, 0)


list_of_reads = range(len(align))
graph = gv.Digraph()
graph.node(str(counter), label='Len of aln '+str(len(align[0])))
build_tree(graph, list_of_reads, counter, 1, True)
graph.save('1.dot', '/home/denis/')

stat_file = open('./tree_stat.txt', "w+")
line = ' '.join(str(i) for i in ls)+'\n'
stat_file.write(line)
stat_file.close()
