import sys
from Bio import AlignIO
import os


def get_stat(dir_name):
    nuc_dict = {"A": 0, "C": 1, "G": 2, "T": 3, "-": 4}

    out = open("/home/denis/Study/nir/nir_aut/overview", "w+")
    out.write("Name\tNumber of reads\tNumber of errors"+'\n')
    for filename in os.listdir(dir_name):
        handle = open(dir_name+filename, "rU")
        align = AlignIO.read(handle, "clustal")
        num_columns = len(align[0])

        error_list = []
        for i in range(num_columns):
            error_list.append([0]*5)

        for i in range(0, num_columns):
            for j in range(0, len(align)):
                char = align[j][i]  # i-th symbol in j-th string
                ind = nuc_dict[char]
                error_list[i][ind] += 1
        errors = 0
        for i in range(num_columns):
            if max(error_list[i]) < 3.0/4.0*len(align) and i < num_columns*9.0/10.0:
                errors += 1
        if errors > 0:
            out.write(filename+'\t'+str(len(align))+'\t'+str(errors)+'\n')
        handle.close()
    out.close()


def main():
    if len(sys.argv) != 2:
        print "Expect to be a dir with clustal files"
        return
    get_stat(sys.argv[1])

if __name__ == "__main__":
    main()
