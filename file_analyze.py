import os.path
import graphviz as gv
from Bio import AlignIO

counter = 0


class Node:
    def __init__(self):
        self.children = {}


def preorder(graph, tail, current_node):
    global counter
    for err_pos in current_node.children:
        for err_nuc in current_node.children[err_pos]:
            label = str(err_pos)+' '+err_nuc+' '+str(current_node.children[err_pos][err_nuc][1])
            counter += 1
            head = str(counter)
            graph.node(head, label)
            graph.edge(tail, head)
            preorder(graph, head, current_node.children[err_pos][err_nuc][0])


def build_tree(cons, error_data):
    root = Node()
    for i in range(len(error_data[0][0])):
        current = root
        for j in range((len(error_data))):
            err_pos = error_data[j][1][2]
            err_nuc = error_data[j][0][i]
            #if cons[err_pos] == err_nuc:
             #   continue
            if err_pos not in current.children:
                new_node = Node()
                current.children[err_pos] = {err_nuc: [new_node, 0]}
            if err_nuc not in current.children[err_pos]:
                new_node = Node()
                current.children[err_pos][err_nuc] = [new_node, 0]

            current.children[err_pos][err_nuc][1] += 1
            current = current.children[err_pos][err_nuc][0]

    graph = gv.Digraph()
    global counter
    counter = 0
    graph.node('root', 'CONSENSUS')
    for err_pos in root.children:
        for err_nuc in root.children[err_pos]:
            counter += 1
            tail = 'root'
            head = str(counter)
            label = str(err_pos)+' '+err_nuc+' '+str(root.children[err_pos][err_nuc][1])
            graph.node(head, label)
            graph.edge(tail, head)
            preorder(graph, head, root.children[err_pos][err_nuc][0])
    graph.save('asd', '/home/denis/')


def parse_overview(file_name):
    l = []
    with open(file_name) as overview:
        next(overview)
        for line in overview:
            info = line.split('\t')
            l.append(info)
    return l


def parse_aln(align_list):
    filename = "/home/denis/Study/nir/nir_aut/clustal_bars/"+align_list[0]
    if not os.path.exists(filename):
        return False

    nuc_dict = {"A": 0, "C": 1, "G": 2, "T": 3, "-": 4}
    num_dict = {0: "A", 1: "C", 2: "G", 3: "T", 4: "-"}

    handle = open(filename, "rU")
    align = AlignIO.read(handle, "clustal")
    num_columns = len(align[0])

    error_list = []
    for i in range(num_columns):
        error_list.append([0]*5)
    for i in range(0, num_columns):
        for j in range(0, len(align)):
            char = align[j][i]  # i-th symbol in j-th string
            ind = nuc_dict[char]
            error_list[i][ind] += 1

    error_strings = []
    consensus = ''

    for i in range(num_columns):
        cur_max = max(error_list[i])
        index = error_list[i].index(cur_max)
        consensus += num_dict[index]
        # i < num_columns
        # sub_max > 1
        if cur_max < len(align) - 2 and 4 < i < num_columns-3:
            second_max = 0
            for ind, val in enumerate(error_list[i]):
                if val>second_max:
                    second_max = val
            if second_max > 1:
                # chars , [max count, char of max count, position in read]
                tmp = [[], [second_max, num_dict[index], i]]
                for j in range(0, len(align)):
                    tmp[0].append(align[j][i])
                error_strings.append(tmp)

    handle.close()
    if len(error_strings) == 0:
        return False

    error_strings = sorted(error_strings, key=lambda item: item[1][0])
    print align_list[0]
    build_tree(consensus, error_strings)
    return True


def main():
    file_name = "/home/denis/Study/nir/nir_aut/overview"
    ov_w = parse_overview(file_name)
    i = 0
    for item in ov_w:
        data = parse_aln(item)
        if data != False:
            i += 1
        if i > 2:
            break

if __name__ == "__main__":
    main()
