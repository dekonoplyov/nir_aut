import glob
from Bio import AlignIO

replace_list = [0] * 16
range_length = [0] * 1000
nuc_dict = {'A':{'A': 0, 'C': 1, 'G': 2, 'T': 3},
            'C':{'A': 4, 'C': 5, 'G': 6, 'T': 7},
            'G':{'A': 8, 'C': 9, 'G': 10, 'T': 11},
            'T':{'A': 12, 'C': 13, 'G': 14, 'T': 15}}

char_dict =  {"A": 0, "C": 1, "G": 2, "T": 3, "-": 4}
num_dict = {0: "A", 1: "C", 2: "G", 3: "T", 4: "-"}

def get_stat(file_name):
    handle = open(file_name, "rU")
    align = AlignIO.read(handle, "clustal")
    num_columns = len(align[0])

    for i in range(0, num_columns):
        error_list = [0]*5
        for j in range(0, len(align)):
            char = align[j][i]  # i-th symbol in j-th string
            ind = char_dict[char]
            error_list[ind] += 1

        cur_max = max(error_list)
        index_max = error_list.index(cur_max)
        if max(error_list) < len(align) - 2:
            range_length[i] += 1
            for bla in range(5):
                fr = num_dict[index_max]
                to = num_dict[bla]
                if fr == to:
                    continue
                replace_list[nuc_dict[fr][to]] += 1

    handle.close()


def main():
    for file in glob.glob("/home/adam/*.aln"):
        get_stat(file)


if __name__ == '__main__':
    main()