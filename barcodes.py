import sys
from Bio import SeqIO
import os.path


def parse_barcode_file(file_name):
    """old function
    """
    actual_barcodes = {}
    with open(file_name) as bar_file:
        for line in bar_file:
            info = line.split()
            yo = info[0].split(":")
            if len(yo) != 12:
                continue
            if yo[len(yo)-2] in actual_barcodes:
                actual_barcodes[yo[len(yo)-2]][1] += 1
            else:
                actual_barcodes[yo[len(yo)-2]] = [info[1], 0]
    return actual_barcodes

def pre_count(file_name):
    actual_barcodes = {}
    with open(file_name, 'rU') as fasta_file:
        for line in SeqIO.parse(fasta_file, "fastq"):
            info = line.id.split()[0]
            yo = info.split(':')
            if len(yo) != 12:
                continue
            if yo[len(yo)-2] in actual_barcodes:
                actual_barcodes[yo[len(yo)-2]][1] += 1
            else:
                actual_barcodes[yo[len(yo)-2]] = [info[1], 0]
    return actual_barcodes


def parse_fasta(file_name, bar_dict):
    with open(file_name, 'rU') as fasta_file:
        for record in SeqIO.parse(fasta_file, "fastq"):
            info = record.id.split()[0]
            yo = info.split(':')
            if len(yo) != 12:
                continue
            if bar_dict[yo[-2]][1] < 15:
                continue
            clus_name = "/home/denis/out/"+yo[-2]+".fastq"
            if os.path.exists(clus_name):
                cluster = open(clus_name, "a")
                SeqIO.write(record, cluster, "fastq")
                cluster.close()
            else:
                cluster = open(clus_name, "w+")
                SeqIO.write(record, cluster, "fastq")
                cluster.close()


def main():
    if len(sys.argv) != 2:
        print "Wrong input"
        print "Input should be like: fastq_file"
        return
    fasta_file = sys.argv[1]
    bar_dict = pre_count(fasta_file)
    parse_fasta(fasta_file, bar_dict)

if __name__ == "__main__":
    main()
